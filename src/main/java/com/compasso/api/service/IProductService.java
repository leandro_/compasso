package com.compasso.api.service;

import java.util.List;

import com.compasso.api.domain.Product;
import com.compasso.api.dto.ProductDTO;

public interface IProductService {

	Product save(ProductDTO dto);
	List<Product> listAll();
	Product findbyUuidOrThrowException(String uuid);
	void delete(String uuid);
	Product replace(String uuid, ProductDTO dto);
}

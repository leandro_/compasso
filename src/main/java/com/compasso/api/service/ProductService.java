package com.compasso.api.service;

import static com.compasso.api.util.StreamUtil.areAllNull;

import java.math.BigDecimal;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.compasso.api.domain.Product;
import com.compasso.api.dto.ProductDTO;
import com.compasso.api.exception.NotFoundException;
import com.compasso.api.repository.ProductRepository;
import com.compasso.api.specs.SpecificationProductSearch;

import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class ProductService {
	
	ProductRepository repository;
	ModelMapper modelMapper;
	
	@Transactional
	public Product save(ProductDTO dto){
		var product = modelMapper.map(dto, Product.class);
		return repository.save(product);
	}

	public List<Product> listAll() {
		return repository.findAll();
	}
	
	public Product findbyUuidOrThrowException(String uuid) {
		return repository.findById(uuid)
				.orElseThrow(() -> new NotFoundException("NOT FOUND"));
	}
	
	public void delete(String uuid){	
		repository.delete(findbyUuidOrThrowException(uuid));
	}
	
	public Product replace(String uuid, ProductDTO dto) {
		findbyUuidOrThrowException(uuid);
		var product = modelMapper.map(dto, Product.class);
		product.setUuid(uuid);
		return repository.save(product);
	}
	
		
	public List<Product> search(String search, BigDecimal minPrice, BigDecimal maxPrice) {
		
		if (areAllNull(search, minPrice, maxPrice))
			return repository.findAll();
		
		return repository.findAll(new SpecificationProductSearch(search, minPrice, maxPrice));
	}
}

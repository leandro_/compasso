package com.compasso.api.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchDTO {

	String q;
	
	@JsonProperty("min_price")
	@Positive(message = "Please, provide a number")
	BigDecimal minPrice;
	
	@JsonProperty("max_price")
	@Positive(message = "Please, provide a number")
	BigDecimal maxPrice;
}

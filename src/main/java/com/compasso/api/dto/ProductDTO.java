package com.compasso.api.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDTO {
	
	@NotBlank(message = "Name of product cannot be empty. ")
	@Length(max = 64, message = "Name should shorter - maximum of 64 characters.")
	@Schema(example = "broom", description = "The name of product", required = true)
	private String name;
	
	@NotBlank(message = "Description of product cannot be empty. ")
	@Length(max = 255, message = "Description should shorter - maximum of 255 characters.")
	@Schema(example = "This is an excellent product to clean your house", description = "The description of product", required = true)
	private String description;
	
	@Positive(message = "Product must have a number positive as a price. ")
	@NotNull(message = "Please provide a price for product. ")
	@Schema(example = "34.20", description = "The price of product", required = true)
	private BigDecimal price;
	
}
package com.compasso.api.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.compasso.api.domain.Product;
import com.compasso.api.dto.ProductDTO;
import com.compasso.api.service.ProductService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/products")
public class ProductController {
	
	ProductService service;

	@GetMapping
	@Operation(summary = "List products", description = "List all registered produts")
	public ResponseEntity<List<Product>> listAll(){
		return ResponseEntity.ok(service.listAll());
	}
	
	@GetMapping("/{uuid}")
	@Operation(summary = "Get one product", description = "Get a specific product using its identifier (uuid) ")
	public ResponseEntity<Product> findById(@PathVariable String uuid){
		return ResponseEntity.ok(service.findbyUuidOrThrowException(uuid));
	}
	
	@PostMapping
	@Operation(summary = "Save a product", description = "Write a product (store) ")
	public ResponseEntity<Product> post(@RequestBody @Valid ProductDTO dto) {
		return new ResponseEntity<>(service.save(dto), HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{uuid}")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "204", description = "Successful operation. No content is returned."),
			@ApiResponse(responseCode = "404", description = "Product was not found in the database.")
	})
	@Operation(summary = "Delete a product", description = "Delete a specific product using its identifier (uuid) ")
	public ResponseEntity<Void> delete(@PathVariable String uuid) {
		service.delete(uuid);
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{uuid}")
	@Operation(summary = "Update a product", description = "Update a product. Send the identifier(uuid) in the url and the attributes on the entity of that sended on body ")
	public ResponseEntity<Product> put(@PathVariable String uuid, @RequestBody ProductDTO dto) {
		return ResponseEntity.ok(service.replace(uuid, dto));
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<Product>> search(@RequestParam(required = false) String q, 
			@RequestParam(required = false, name = "min_price") BigDecimal minPrice, 
			@RequestParam(required = false, name = "max_price") BigDecimal maxPrice)
	{
		return ResponseEntity.ok(service.search(q, minPrice, maxPrice));
	}
}

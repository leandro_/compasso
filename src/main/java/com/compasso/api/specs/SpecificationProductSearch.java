package com.compasso.api.specs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.compasso.api.domain.Product;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
public class SpecificationProductSearch implements Specification<Product> {

	private static final long serialVersionUID = 1L;
	private String search;
	private BigDecimal minPrice;
	private BigDecimal maxPrice;
	
	@Override
	public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		Optional.ofNullable(search).ifPresent(
			s -> {
				var predicatesSearchName = criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + s.toLowerCase() + "%");
				var predicatesSearchDescription = criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + s.toLowerCase() + "%");
				predicates.add(criteriaBuilder.and(criteriaBuilder.or(predicatesSearchName, predicatesSearchDescription)));
			}
		);

		Optional.ofNullable(minPrice).ifPresent(
			s -> predicates.add(criteriaBuilder.and(criteriaBuilder.greaterThanOrEqualTo(root.get("price"), minPrice)))
		);
		
		Optional.ofNullable(maxPrice).ifPresent(
			s -> predicates.add(criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(root.get("price"), maxPrice)))
		);
	      
		return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
	}
	
}
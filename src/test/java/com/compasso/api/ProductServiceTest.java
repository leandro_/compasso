package com.compasso.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.compasso.api.domain.Product;
import com.compasso.api.dto.ProductDTO;
import com.compasso.api.exception.NotFoundException;
import com.compasso.api.repository.ProductRepository;
import com.compasso.api.service.ProductService;
import com.compasso.api.util.ProductUtilTest;

@ExtendWith(SpringExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
class ProductServiceTest {

	@InjectMocks
	private ProductService productService;
	
	@Mock
	ProductRepository productRepositoryMock;
	
	@Mock 
	ModelMapper modelMapperMock;
	
	@BeforeAll
	void init() {

		ProductDTO productDTOToBeSaved = ProductUtilTest.createProductDTOToBeSave();
		ProductDTO productDTOToBeUpdate = ProductUtilTest.createProductDTOToBeUpdate();
		Product productToBeSaved = ProductUtilTest.createProductToBeSave();
		Product productToBeUpdate = ProductUtilTest.createProductToBeUpdate();
		
		Product validProduct = ProductUtilTest.createProductValid();
		
		BDDMockito.when(productRepositoryMock.save(ProductUtilTest.createProductToBeSave()))
		.thenReturn(ProductUtilTest.createProductSaved());
		
		BDDMockito.when(productRepositoryMock.save(productToBeUpdate))
		.thenReturn(ProductUtilTest.createProductUpdated());
		
		BDDMockito.when(modelMapperMock.map(productDTOToBeSaved, Product.class))
		.thenReturn(productToBeSaved);
		
		BDDMockito.when(modelMapperMock.map(productDTOToBeUpdate, Product.class))
		.thenReturn(productToBeUpdate);
		
		BDDMockito.when(productRepositoryMock.findAll())
		.thenReturn(List.of(validProduct));

		BDDMockito.when(productRepositoryMock.findById(ProductUtilTest.ID_VALID))
		.thenReturn(Optional.of(validProduct));
		
		BDDMockito.when(productRepositoryMock.findById(ProductUtilTest.ID_PRODUCT_NOT_FOUND))
		.thenReturn(Optional.empty());
		
		BDDMockito.doNothing().when(productRepositoryMock).delete(ProductUtilTest.createProductValid());
	}

	@Test
	@DisplayName("Test Service Product - save")
	void testServiceProductSave() {
		ProductDTO productDTOToBeSaved = ProductUtilTest.createProductDTOToBeSave();
		Product productSaved = productService.save(productDTOToBeSaved);
		assertThat(productSaved).isNotNull().isEqualTo(ProductUtilTest.createProductSaved());
	}
	
	@Test
	@DisplayName("Test Service Product - listAll")
	void testServiceProductListAll() {
		List<Product> list = productService.listAll();
		assertThat(list).isNotEmpty().hasSize(1);
		assertThat(list.get(0).getUuid()).isEqualTo(ProductUtilTest.createProductValid().getUuid());
	}
	
	@Test
	@DisplayName("Test Service Product - findbyUuidOrThrowException")
	void testServiceProductFindProductFound() {
		Product findProduct = productService.findbyUuidOrThrowException(ProductUtilTest.ID_VALID);
		assertThat(findProduct).isNotNull().isEqualTo(ProductUtilTest.createProductValid());
	}
	
	@Test
	@DisplayName("Test Service Product - findbyUuidOrThrowException - throw NotFoundException")
	void testServiceProductFindProductNotFound() {
		Assertions.assertThatThrownBy(() -> productService.findbyUuidOrThrowException(ProductUtilTest.ID_PRODUCT_NOT_FOUND))
				.isInstanceOf(NotFoundException.class);
	}
	
	@Test
	@DisplayName("Test Service Product - delete")
	void testServiceProductDelete() {
		
		Assertions.assertThatCode(() -> productService.delete(ProductUtilTest.ID_VALID))
			.doesNotThrowAnyException();
		
		Assertions.assertThatThrownBy(() -> productService.delete(ProductUtilTest.ID_PRODUCT_NOT_FOUND))
		.isInstanceOf(NotFoundException.class);
	}
	
	@Test
	@DisplayName("Test Service Product - replace ")
	void testControllerProductPut() {
		ProductDTO dto = ProductUtilTest.createProductDTOToBeUpdate();
		Product productSaved = productService.replace(ProductUtilTest.ID_VALID, dto);
		assertThat(productSaved).isNotNull().isEqualTo(ProductUtilTest.createProductUpdated());
	}
}

package com.compasso.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.compasso.api.domain.Product;
import com.compasso.api.repository.ProductRepository;
import com.compasso.api.util.ProductUtilTest;

@DataJpaTest
@DisplayName("Tests for Product Repository")
class ProductRepositoryTest {

	@Autowired
	private ProductRepository repository;
	
	@Test
	@DisplayName("Repository Product - save")
	void saveProductWhenSuccess() {
		Product product = ProductUtilTest.createProductToBeSave();
		Product productSaved = repository.save(product);
		assertThat(productSaved).isNotNull();
		assertThat(productSaved.getUuid()).isNotBlank();
		assertEquals(product.getName(), productSaved.getName());
	}
	
	@Test
	@DisplayName("Repository Product - update")
	void updateProductWhenSuccess() {
		Product product = ProductUtilTest.createProductToBeSave();
		Product productSaved = repository.save(product);
		productSaved.setName("new name");
		Product productUpdated = repository.save(product);
		
		assertThat(productUpdated).isNotNull();
		assertThat(productUpdated.getUuid()).isNotBlank();
		assertEquals(productUpdated.getName(), productSaved.getName());
	}
	
	@Test
	@DisplayName("Repository Product - delete")
	void deleteProductWhenSuccess() {
		Product product = ProductUtilTest.createProductToBeSave();
		Product productSaved = repository.save(product);
		assertThat(productSaved.getUuid()).isNotBlank();
		
		repository.delete(productSaved);
		Optional<Product> productOptional = repository.findById(productSaved.getUuid());
		assertThat(productOptional).isNotPresent();
	}
	
	@Test
	@DisplayName("Repository Product - findById - notFound")
	void findByIdProductThenNotFound() {
		String strangeId = "strangeThatNotExist";
		Optional<Product> productOptional = repository.findById(strangeId);
		assertThat(productOptional).isNotPresent();
	}
}

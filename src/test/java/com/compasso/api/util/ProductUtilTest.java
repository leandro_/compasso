package com.compasso.api.util;

import java.math.BigDecimal;

import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.compasso.api.domain.Product;
import com.compasso.api.dto.ProductDTO;

public class ProductUtilTest {

	public static final String ID_VALID = "ID";
	public static final String ID_PRODUCT_NOT_FOUND = "ID_PRODUCT_NOT_FOUND";
	
	private static final String NAME = "product1";
	private static final String DESCRIPTION = "description1";
	
	private static final String NAME_PRE_UPDATE = "product1-will-be-update";
	private static final String NAME_UPDATED = "product-updated";
	
	private static final BigDecimal PRICE = BigDecimal.valueOf(1.1);
	
	public static ProductDTO createProductDTOToBeSave() {
		return ProductDTO.builder()
				.name(NAME)
				.description(DESCRIPTION)
				.price(PRICE)
				.build();
	}
	
	public static Product createProductToBeSave() {
		return Product.builder()
				.name(NAME)
				.description(DESCRIPTION)
				.price(PRICE)
				.build();
	}
	
	public static ProductDTO createProductDTOToBeUpdate() {
		return ProductDTO.builder()
				.name(NAME_PRE_UPDATE)
				.description(DESCRIPTION)
				.price(PRICE)
				.build();
	}
	
	public static Product createProductToBeUpdate() {
		return Product.builder()
				.name(NAME_PRE_UPDATE)
				.description(DESCRIPTION)
				.price(PRICE)
				.build();
	}
	
	public static Product createProductSaved() {
		return Product.builder()
				.uuid(ID_VALID)
				.name(NAME)
				.description(DESCRIPTION)
				.price(PRICE)
				.build();
	}

	public static Product createProductUpdated() {
		return Product.builder()
				.uuid(ID_VALID)
				.name(NAME_UPDATED)
				.description(DESCRIPTION)
				.price(PRICE)
				.build();
	}
	
	public static Product createProductValid() {
		return Product.builder()
				.uuid(ID_VALID)
				.name(NAME)
				.description(DESCRIPTION)
				.price(PRICE)
				.build();
	}
	
	public static Product createProductValidUpdate() {
		return Product.builder()
				.name("product2")
				.description("description2")
				.price(BigDecimal.valueOf(2.20))
				.build();
	}
	
	public static MultiValueMap<String, String> headerJson() {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", MediaType.APPLICATION_JSON.toString());
		return headers;
	}
}

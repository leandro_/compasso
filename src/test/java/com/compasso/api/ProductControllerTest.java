package com.compasso.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.compasso.api.controller.ProductController;
import com.compasso.api.domain.Product;
import com.compasso.api.dto.ProductDTO;
import com.compasso.api.service.ProductService;
import com.compasso.api.util.ProductUtilTest;

@ExtendWith(SpringExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
class ProductControllerTest {

	@InjectMocks
	private ProductController productController;
	
	@Mock
	private ProductService productServiceMock;
	
	@BeforeAll
	void init() {

		List<Product> lista = List.of(ProductUtilTest.createProductValid());
		BDDMockito.when(productServiceMock.listAll()).thenReturn(lista);
		
		BDDMockito.when(productServiceMock.findbyUuidOrThrowException(ArgumentMatchers.any()))
				  .thenReturn(ProductUtilTest.createProductValid());
		
		BDDMockito.when(productServiceMock.save(ProductUtilTest.createProductDTOToBeSave()))
		  .thenReturn(ProductUtilTest.createProductSaved());
		
		BDDMockito.when(productServiceMock.replace("uuidEntityReplace", ProductUtilTest.createProductDTOToBeUpdate()))
		  .thenReturn(ProductUtilTest.createProductUpdated());
	}
	
	@Test
	@DisplayName("Test Controller Product - listAll ")
	void testControllerProductListAll() {
		ResponseEntity<?> response = productController.listAll();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isEqualTo(List.of(ProductUtilTest.createProductValid()));
	}
	
	@Test
	@DisplayName("Test Controller Product - findbyId ")
	void testControllerProductFindById() {
		ResponseEntity<?> response = productController.findById("xxx");
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isEqualTo(ProductUtilTest.createProductValid());
	}
	
	@Test
	@DisplayName("Test Controller Product - post ")
	void testControllerProductPost() {
		ResponseEntity<?> response = productController.post(ProductUtilTest.createProductDTOToBeSave());
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(response.getBody()).isEqualTo(ProductUtilTest.createProductSaved());
	}
	
	@Test
	@DisplayName("Test Controller Product - delete ")
	void testControllerProductDelete() {
		ResponseEntity<?> response = productController.delete(" ");
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
	}
	
	@Test
	@DisplayName("Test Controller Product - put ")
	void testControllerProductPut() {
		ProductDTO dto = ProductUtilTest.createProductDTOToBeUpdate();
		ResponseEntity<?> response = productController.put("uuidEntityReplace", dto);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody()).isEqualTo(ProductUtilTest.createProductUpdated());
	}	
}

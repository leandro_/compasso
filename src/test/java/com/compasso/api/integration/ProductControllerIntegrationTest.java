package com.compasso.api.integration;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.compasso.api.domain.Product;
import com.compasso.api.dto.ProductDTO;
import com.compasso.api.util.ProductUtilTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
@TestInstance(Lifecycle.PER_CLASS)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class ProductControllerIntegrationTest {
	
	@Autowired
	private TestRestTemplate testRestTemplate;
	
	private final String URL_BASE = "/products";
	private final String URL_SEARCH = URL_BASE + "/search";
	
	ObjectMapper objectMapper = new ObjectMapper();
	
	@Test
	@DisplayName("Test Product Integration - list")
	void list() {
		ResponseEntity<List<Product>> responseEntity = 
				testRestTemplate.exchange(
				    URL_BASE,
				    HttpMethod.GET,
				    null,
				    new ParameterizedTypeReference<List<Product>>() {}
				);
		List<Product> products = responseEntity.getBody();
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(products).isInstanceOf(List.class);
	}
	
	@Test
	@DisplayName("Test Product Integration - post")
	void save() throws JsonProcessingException {
		
		ProductDTO dto = ProductUtilTest.createProductDTOToBeSave();
		ResponseEntity<Product> response = saveProduct(dto);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(response.getBody().getName()).isEqualTo(dto.getName());
		assertThat(response.getBody().getUuid()).isNotNull();
	}

	@Test
	@DisplayName("Test Product Integration - findById")
	void findById() throws JsonProcessingException {
		
		ProductDTO dto = ProductUtilTest.createProductDTOToBeSave();
		ResponseEntity<Product> response = saveProduct(dto);
		String uuid = response.getBody().getUuid();
		
		ResponseEntity<Product> responseEntity = testRestTemplate
				.exchange(URL_BASE + "/" + uuid, HttpMethod.GET, null, Product.class);
		
		Product productFound = responseEntity.getBody();
		
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(productFound).isNotNull();
		assertThat(productFound.getUuid()).isEqualTo(uuid);
	}
	
	@Test
	@DisplayName("Test Product Integration - delete")
	void delete() throws JsonProcessingException {
		
		ProductDTO dto = ProductUtilTest.createProductDTOToBeSave();
		ResponseEntity<Product> response = saveProduct(dto);
		
		ResponseEntity<Void> responseEntity = testRestTemplate
				.exchange(URL_BASE + "/" + response.getBody().getUuid(), HttpMethod.DELETE, null, Void.class);
		
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
		assertThat(responseEntity.getBody()).isNull();
	}
	
	@Test
	@DisplayName("Test Product Integration - put")
	void put() throws JsonProcessingException {
		
		ProductDTO dto = ProductUtilTest.createProductDTOToBeSave();
		ResponseEntity<Product> responseSave = saveProduct(dto);
		Product product = responseSave.getBody();
		
		String newName = "other name";
		product.setName(newName);
		ResponseEntity<Product> responseUpdate = putObject(product);
		
		assertThat(responseUpdate.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(responseUpdate.getBody().getName()).isEqualTo(newName);
		assertThat(product.getUuid()).isNotNull();
	}
	
	@Test
	@DisplayName("Test Product Integration - search with q")
	void searchUsingQ() throws JsonProcessingException {

		ProductDTO dto1, dto2, dto3;
		dto1 = ProductDTO.builder().name("one").description("abh").price(BigDecimal.valueOf(5)).build();
		dto2 = ProductDTO.builder().name("two").description("cdc").price(BigDecimal.valueOf(10)).build();
		dto3 = ProductDTO.builder().name("three").description("pwp").price(BigDecimal.valueOf(15)).build();
		savePreSearch(dto1, dto2, dto3);
		
		UriComponentsBuilder builderURISearchW = UriComponentsBuilder.fromPath(URL_SEARCH).queryParam("q", "W");
		UriComponentsBuilder builderURISearchE = UriComponentsBuilder.fromPath(URL_SEARCH).queryParam("q", "E");
		UriComponentsBuilder builderURISearchP = UriComponentsBuilder.fromPath(URL_SEARCH).queryParam("q", "p");
		
		ResponseEntity<List<Product>> response1 = testRestTemplate.exchange(builderURISearchW.toUriString(), 
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});
		
		ResponseEntity<List<Product>> response2 = testRestTemplate.exchange(builderURISearchE.toUriString(), 
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});
		
		ResponseEntity<List<Product>> response3 = testRestTemplate.exchange(builderURISearchP.toUriString(), 
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});

		List<String> listResponse1 = response1.getBody().stream().map(Product::getName).collect(Collectors.toList());
		List<String> listResponse2 = response2.getBody().stream().map(Product::getName).collect(Collectors.toList());
		List<String> listResponse3 = response3.getBody().stream().map(Product::getName).collect(Collectors.toList());
		
		assertThat(listResponse1).hasSize(2).contains(dto2.getName(), dto3.getName());
		
		assertThat(listResponse2).hasSize(2)
			.contains(dto1.getName(), dto3.getName());
		
		assertThat(listResponse3).hasSize(1).contains(dto3.getName());
	}
	
	@Test
	@DisplayName("Test Product Integration - search with minPrice, maxPrice")
	void searchUsingPrices() throws JsonProcessingException {

		ProductDTO dto1, dto2, dto3;
		dto1 = ProductDTO.builder().name("one").description("abh").price(BigDecimal.valueOf(5)).build();
		dto2 = ProductDTO.builder().name("two").description("cdc").price(BigDecimal.valueOf(10)).build();
		dto3 = ProductDTO.builder().name("three").description("pwp").price(BigDecimal.valueOf(15)).build();
		savePreSearch(dto1, dto2, dto3);
		
		UriComponentsBuilder builderURISearch1 = UriComponentsBuilder.fromPath(URL_SEARCH)
				.queryParam("min_price", BigDecimal.valueOf(4))
				.queryParam("max_price", BigDecimal.valueOf(10));
		
		UriComponentsBuilder builderURISearch2 = UriComponentsBuilder.fromPath(URL_SEARCH).queryParam("min_price", BigDecimal.valueOf(16));
		UriComponentsBuilder builderURISearch3 = UriComponentsBuilder.fromPath(URL_SEARCH).queryParam("max_price", BigDecimal.valueOf(15));

		ResponseEntity<List<Product>> response1 = testRestTemplate.exchange(builderURISearch1.toUriString(), 
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});
		
		ResponseEntity<List<Product>> response2 = testRestTemplate.exchange(builderURISearch2.toUriString(), 
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});
		
		ResponseEntity<List<Product>> response3 = testRestTemplate.exchange(builderURISearch3.toUriString(), 
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {});

		List<String> listResponse1 = response1.getBody().stream().map(Product::getName).collect(Collectors.toList());
		List<String> listResponse2 = response2.getBody().stream().map(Product::getName).collect(Collectors.toList());
		List<String> listResponse3 = response3.getBody().stream().map(Product::getName).collect(Collectors.toList());
		
		assertThat(listResponse1).hasSize(2).contains(dto1.getName(), dto2.getName());
		
		assertThat(listResponse2).isEmpty();
		
		assertThat(listResponse3).hasSize(3)
		.contains(dto1.getName(), dto2.getName(), dto3.getName());
	}

	/**
	 * @param dto1
	 * @param dto2
	 * @param dto3
	 * @throws JsonProcessingException
	 */
	@Transactional
	public void savePreSearch(ProductDTO dto1, ProductDTO dto2, ProductDTO dto3) throws JsonProcessingException {
		saveProduct(dto1);
		saveProduct(dto2);
		saveProduct(dto3);
	}
	
	private ResponseEntity<Product> saveProduct(ProductDTO dto) throws JsonProcessingException {
		MultiValueMap<String, String> headers = ProductUtilTest.headerJson();
		String jsonStr = objectMapper.writeValueAsString(dto);
		HttpEntity<String> request = new HttpEntity<String>(jsonStr, headers);
		ResponseEntity<Product> responseEntity = testRestTemplate
				.exchange(URL_BASE, HttpMethod.POST, request, Product.class);
		return responseEntity;
	}
	
	private ResponseEntity<Product> putObject(Product product) throws JsonProcessingException {
		MultiValueMap<String, String> headers = ProductUtilTest.headerJson();
		String jsonStr = objectMapper.writeValueAsString(product);
		HttpEntity<String> request = new HttpEntity<String>(jsonStr, headers);
		ResponseEntity<Product> responseEntity = testRestTemplate
				.exchange(URL_BASE + "/" + product.getUuid(), HttpMethod.PUT, request, Product.class);
		return responseEntity;
	}
}

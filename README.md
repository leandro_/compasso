---

## Tarefa de Leandro Souza - prova técnica Compasso UOL

Projeto elaborado utilizando Spring Boot. 

Características:  
- Banco de dados **MySQL em nuvem** na Amazon RDS para facilitar a execução. O banco criado ficará disponível enquanto a tarefa estiver sendo avaliada.  
- Banco de dados em memória (H2) para execução de testes  
- Porta definda como 9999 conforme especificado  
- O projeto foi feito em Java versão 15  
- Projeto criado com maven  
- Testes desenvolvidos com JUnit 5 e Spring para: controllers, service, repository, e integração  
- Para melhor visualização dos endpoints, após a execução, acessar [http://localhost:9999/doc](http://localhost:9999/doc). Foi desenvolvido documentação com **Open API (Swagger)**  
- Foram utilizados IDE Eclipse e SonarLint para desenvolvimento da tarefa  

---

## Baixar o projeto
Clonar utilizando:  
`git clone git@bitbucket.org:leandro_/compasso.git`

Acessar a pasta do projeto compasso:  
`cd compasso`

## Execução

### Execução simples via Docker

Criar o jar com:  
`mvn package`

Gerar a imagem docker com:   
`docker build --tag=api:latest .`  

Executar a imagem:  
`docker run -p9999:9999 api:latest`

### Alternativa utilizando Buildpacks
Este é um método alternativo de execução. Não precisa ser executado em conjunto com o método anterior.
Desde a versão 2.3.x o Spring suporta [buildpacks](https://buildpacks.io/)
Para utilizar basta executar conforme abaixo:  

`
mvn spring-boot:build-image
`

Neste caso é gerado o jar (todos os testes também são executados) e a imagem docker logo depois. O processo pode demorar alguns minutos.

Logo depois pode ser executado a imagem com:  

`
docker run -p9999:9999 api:0.0.1-SNAPSHOT
`

## Testes
### Classes de Teste e execução via maven
Foram criadas as seguintes classes de teste:  

`ProductControllerTest.java` `ProductRepositoryTest.java` `ProductService.java` `ProductControllerIntegrationTest.java`

Os testes podem ser executados via maven utilizando: `mvn test`

Foi criado um profile específico (no `pom.xml`) para execução do teste de integração. Caso seja objetivo executar somente esse teste, utilizar o comando abaixo:  
`mvn test -P integration-test`

Poderia ser criado profile específico para tipos diferentes de testes.

## Outras informações

### Dados de conexão com banco MySQL
Caso queira conectar com o banco de dados, usando algum client, poderá usar os dados abaixo:  

`URL: database-leandro-product.cqx7klnq2q33.us-east-1.rds.amazonaws.com`  
`Database: productcompasso`  
`username: admin`  
`password: yrKZ4Du0lJjfPYlUV0uI`  

Contato:  
<sup>Leandro Alves de Souza - leandro.dev@icloud.com - (37) 9 91942910</sup>

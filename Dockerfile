FROM openjdk:15
LABEL maintainer="leandrosouza"
COPY target/api-0.0.1-SNAPSHOT.jar api-0.0.1.jar
ENTRYPOINT ["java","-jar","/api-0.0.1.jar"]